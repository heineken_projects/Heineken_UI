#               Python script to build webtester framework
# This script constructs a classpath by including selenium and other dependencies 
# and builds the framework
# Auther : Juby George

import os
from os import listdir
from os.path import isfile, join
import subprocess


#Lists all the jar files in the given path
def scanForJar(path):
  reportFiles = [] 
  fileList = [f for f in listdir(path) if isfile(join(path, f))]
  for file in fileList:
    if file.endswith(".jar"):
      reportFiles.append(file)
  return reportFiles

  
#Add selenium to the classpath
def addSelenium2ClassPath():  
  print("addSelenium2ClassPath")
  classpath = os.getenv('CLASSPATH')#get the classpath environment variable  
  if (classpath == None):
    classpath ="."
  classpath = classpath+";" #append a ; to the end
  cwd       = os.getcwd() #get the absolute path to current folder
  absPath   = os.path.join(cwd,"libs","selenium-2.53.1","libs") #construct path to selenium libs
  jarList   = scanForJar(absPath)#get the list of jars in selenium lib folder
  #print(jarList)
  #Now add the absolute paths of all jars we got to classpath
  for jar in jarList:
    classpath = classpath+absPath+"\\"+jar+";"
  #get absolute path to selenium folder
  absPath   = os.path.join(cwd,"libs","selenium-2.53.1")
  jarList   = scanForJar(absPath)#scan for the jar's in selenium folder
  #print(jarList)
  # Add these jar's too to the classpath
  for jar in jarList:
    classpath = classpath+absPath+"\\"+jar+";"
  #print(classpath) 
  return  classpath

print("Starting Build Script")
#Get the classpath to be used 
webTestercp = addSelenium2ClassPath()   
#print("webTestercp ="+webTestercp)

cwd           = os.getcwd()
JAVAC         = os.getenv('JAVA_HOME')+ "\\bin\\javac.exe" # Construct fully-qualified path of javac
#print("JAVAC = "+ JAVAC)
output        = cwd + "\\out" # Folder where class files to be stored
#Construct absolute path of different packages in WebTester framework
browserPkg    = cwd +'\\test\\techgentsia\\browser\\*.java'
commonPkg     = cwd +'\\test\\techgentsia\\common\\*.java'
cfgPkg        = cwd +'\\test\\techgentsia\\config\\*.java'
testCasesPkg  = cwd +'\\test\\techgentsia\\testcases\\*.java'
UtilsPkg      = cwd +'\\test\\techgentsia\\utils\\*.java'
#print(webTestercp)

#If out folder does not exist, create it
if not os.path.exists(output):
  os.makedirs(output)  

#Build
subprocess.call([JAVAC, '-cp',webTestercp,'-d',output,UtilsPkg,browserPkg,commonPkg,cfgPkg,testCasesPkg])


JAVA          = os.getenv('JAVA_HOME')+ "\\bin\\java.exe" # Construct fully-qualified path of javac
