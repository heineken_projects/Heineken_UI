package test.techgentsia.testcases;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import test.techgentsia.common.BaseTest;
import test.techgentsia.common.HeinekenException;
import test.techgentsia.config.ConfigManager;

public class Staging extends BaseTest
{
  
  private static final int WAIT_TIMEOUT = 1000; /* 0.5 minutes in terms of 500 milliseconds*/
  private String windowHandle;
   
  public Staging( ConfigManager cfgMgr  )
  {
    /* Invoke the BaseTest Class constructor with  
    /* config manager and TAG */
    super(cfgMgr , "Staging");
  }

  private void openAgegatepage(WebDriver driver ,String baseURL )
  {
    driver.get(baseURL);
  } /* end openAgegatepage() */


  private boolean verifyAgegatePage( WebDriver driver )
  { 
    boolean bStatus;
    By      byElement;
    
    byElement = (By)By.xpath("//*[@id=\"agegate\"]/div/div/h1");
    bStatus   = waitForPageToLoad( driver , byElement, WAIT_TIMEOUT );
    return bStatus;
    
   }/* end verifyAgegatePage() */
  
  private void clickonTermsOfUse(WebDriver driver)
  {
    WebElement termsofusebutton;
    termsofusebutton = driver.findElement(By.xpath("html/body/div[1]/div/div/div/div[1]/div/div/form/div[2]/p/a[1]"));
    termsofusebutton.click();
    
  }
  
  private boolean isTermsOfUsePageReached( WebDriver driver )
  { 
    boolean bStatus;
    By      byElement;
    
    byElement = (By)By.xpath("/html/body/div[3]/div[2]/div[1]/ul/li[1]/a");
    bStatus   = waitForPageToLoad( driver , byElement, WAIT_TIMEOUT );
    
   
    String    actualTitle;
    String    expectedTitle;
    
    actualTitle   = driver.getTitle();
    //dbgPrint(actualTitle);
    expectedTitle = "Heineken | Read the small print";
    if(expectedTitle.equals( actualTitle ))
    {
      bStatus = true;
      //dbgPrint(expectedTitle);
    }
    else
    {
      bStatus =  false;
    }
    return bStatus;
   }
  
  private void gobacktoAgegate(WebDriver driver)
  {
    driver.navigate().back();
  }
  
  private void clickonPrivacyPolicy(WebDriver driver)
  {
    WebElement termsofusebutton;
    termsofusebutton = driver.findElement(By.xpath("html/body/div[1]/div/div/div/div[1]/div/div/form/div[2]/p/a[1]"));
    termsofusebutton.click();   
  }
  private boolean isPrivacyPolicyPageReached( WebDriver driver )
  { 
    boolean bStatus;
    By      byElement;
    
    byElement = (By)By.xpath("/html/body/div[3]/div[2]/div[1]/ul/li[2]/a");
    bStatus   = waitForPageToLoad( driver , byElement, WAIT_TIMEOUT );
    
   
    String    actualTitle;
    String    expectedTitle;
    
    actualTitle   = driver.getTitle();
    //dbgPrint(actualTitle);
    expectedTitle = "Heineken | Read the small print";
    if(expectedTitle.equals( actualTitle ))
    {
      bStatus = true;
      //dbgPrint(expectedTitle);
    }
    else
    {
      bStatus =  false;
    }
    return bStatus;
  }
  
  private void tryInvaildAge(WebDriver driver)
  {
    WebElement mm;
    WebElement dd;
    WebElement yy;
    WebElement enter;   
    
    mm = driver.findElement(By.xpath("//*[@id=\"month\"]"));
    mm.clear();
    mm.sendKeys("01");
    dd = driver.findElement(By.xpath("//*[@id=\"day\"]"));
    dd.clear();
    dd.sendKeys("01");
    yy = driver.findElement(By.xpath("//*[@id=\"year\"]"));
    yy.clear();
    yy.sendKeys("2011"); 
    enter = driver.findElement(By.xpath("//*[@id=\"input-date-submit\"]"));
    enter.click();
    
  }/* end tryInvaildAge() */
 
  private boolean checkInvaildAgeerror(WebDriver driver)
  {
    
    String errortoast;
    boolean bStatus;
    By      byElement;
    
    byElement = (By)By.xpath("/html/body/div/div/div/div/div[1]/div/div/form/div[4]/span");
    bStatus   = waitForPageToLoad( driver , byElement, WAIT_TIMEOUT );
    
    errortoast = driver.findElement(By.xpath("/html/body/div/div/div/div/div[1]/div/div/form/div[4]/span")).getText();
    dbgPrint("error toast is : "+errortoast);
    
    if(errortoast.equals("Sorry, come back when you�re of legal drinking age. We�ll keep the Heineken cold."))
    {
      bStatus = true;
    }
    else
    {
      bStatus = false;
    }
    return bStatus;
    
  }/* end checkInvaildAgeerror() */
  
  private void tryVaildAge(WebDriver driver)
  {
    WebElement mm;
    WebElement dd;
    WebElement yy;
    WebElement enter; 
    
    mm = driver.findElement(By.xpath("//*[@id=\"month\"]"));
    mm.clear();
    mm.sendKeys("12");
    dd = driver.findElement(By.xpath("//*[@id=\"day\"]"));
    dd.clear();
    dd.sendKeys("12");
    yy = driver.findElement(By.xpath("//*[@id=\"year\"]"));
    yy.clear();
    yy.sendKeys("1988");   
    enter = driver.findElement(By.xpath("//*[@id=\"input-date-submit\"]"));
    enter.click();
  }/* end tryVaildAge() */
  
  private boolean isHomePageLoaded( WebDriver driver )
  { 
    boolean bStatus;
    By      byElement;
    
    byElement = (By)By.xpath("/html/body/div/main-navbar/div[2]/nav[1]/header/div/div/strong/a/img");
    bStatus   = waitForPageToLoad( driver , byElement, WAIT_TIMEOUT );
    return bStatus;
    
   }/* end verifyHomePage() */
  
   private void clickOnJoinorLogin(WebDriver driver)
   {
     WebElement joinloginbtn;
                                              
     joinloginbtn = driver.findElement(By.xpath("/html/body/div[1]/main-navbar/div[2]/nav[1]/header/div/div/div[1]/div[2]/a"));
     joinloginbtn.click();
    }
   private void isLoginWindowPopupSuccess(WebDriver driver)
   {
     boolean bStatus;
     By      byElement;
     
     byElement = (By)By.xpath("/html/body/div[3]/div[2]/div[2]/div/div/div/form/div[4]/a");
     bStatus   = waitForPageToLoad( driver , byElement, WAIT_TIMEOUT );
   }
   private void clickOnJoinNowButton(WebDriver driver)
   {
     WebElement joinnowbtn;
     joinnowbtn = driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[2]/div/div/div/form/div[4]/a"));
     joinnowbtn.click();
   }
     
   private void isJoinNowWindowPopupSuccess(WebDriver driver)
   {
     boolean bStatus;
     By      byElement;
    
     byElement = (By)By.xpath("//*[@id=\"RegisterForm-dobMonth\"]");
     bStatus   = waitForPageToLoad( driver , byElement, WAIT_TIMEOUT );
   }
   /*
   private boolean checkDOBValues(WebDriver driver)
   {
     boolean bStatus;
     int mm;
     int dd;
     int yy;
     
   
     String actualValString = driver.findElement(By.xpath("//*[@id='post-body-8228718889842861683']/div[1]/table/tbody/tr[1]/td[2]")).getText();
     //To convert actual value string to Integer value.
     int actualVal = Integer.parseInt(actualValString);
     
     mm = Integer.parseInt(driver.findElement(By.xpath("//*[@id=\"RegisterForm-dobMonth\"]")).getText());    
     dd = Integer.parseInt(driver.findElement(By.xpath("//*[@id=\"RegisterForm-dobDay\"]")).getText());
     yy = Integer.parseInt(driver.findElement(By.xpath("//*[@id=\"RegisterForm-dobYear\"]")).getText());
     /*
     if(compareIntegerVals(mm, 12))
     {
    
       bStatus = true;
         }
     else
     {
       bStatus = false;
     }
     
     return bStatus;
     
   }
   */
 
    
  
  @Override
  public void runTests() throws HeinekenException
  {
    WebDriver   driver;
    boolean     bStatus;

    /* Get a driver handle */
    driver  = browserInstance.getDriverInstance();
    
    //Open Agegate page
    dbgPrint("Opening Agegate page...");
    openAgegatepage(driver , cfgMgr.urlUnderTest1);
    //dbgPrint("Waiting for Agegate page to load");
    
    //check Agegate page
    bStatus = verifyAgegatePage(driver);
    rptGen.assertResult("1. Agegate Page Loading ", bStatus);
    assertOnFailure("1. Failed to Load Agegate Page", bStatus);
    dbgPrint("Verified Agegate page");
    
    //Terms of use link testing
    dbgPrint("Opening Terms of use page...");
    clickonTermsOfUse(driver);
    bStatus = isTermsOfUsePageReached(driver);
    rptGen.assertResult("2. Terms of use page Loading ", bStatus);
    assertOnFailure("2. Failed to Load Terms of use Page", bStatus);
    dbgPrint("Verified Terms of use page");
    
    //Navigate back to agegate page
    dbgPrint("Go back to agegate page...");
    gobacktoAgegate(driver);
    bStatus = verifyAgegatePage(driver);
    rptGen.assertResult("3. Back to agegate from terms of use ", bStatus);
    assertOnFailure("3. Failed to go back to agegate from terms of use", bStatus);
    dbgPrint("Verified Agegate page");
   
   //Privacy policy link testing
    dbgPrint("Opening Terms of use page...");
    clickonPrivacyPolicy(driver);
    bStatus = isPrivacyPolicyPageReached(driver);
    rptGen.assertResult("4. Privacy policy page Loading ", bStatus);
    assertOnFailure("4. Failed to Load Privacy policy page", bStatus);
    dbgPrint("Verified Privacy policy page");
    
    //Navigate back to agegate page
    dbgPrint("Go back to agegate page...");
    gobacktoAgegate(driver);
    bStatus = verifyAgegatePage(driver);
    rptGen.assertResult("5. Back to agegate from terms of use ", bStatus);
    assertOnFailure("5. Failed to go back to agegate from terms of use", bStatus);
    dbgPrint("Verified Agegate page");
    
    //Provide invalid age and get error
    dbgPrint("testing agegate with valid age...");
    tryInvaildAge(driver);
    bStatus = checkInvaildAgeerror(driver);
    rptGen.assertResult("6. Agegate testing with invalid age ", bStatus);
    assertOnFailure("6. Failed to test Agegate testing with invalid age", bStatus);    
    
    //provide valid age and go to home page
    dbgPrint("testing agegate with valid age...");
    tryVaildAge(driver);
    bStatus = isHomePageLoaded(driver);
    rptGen.assertResult("7. Agegate testing with valid age ", bStatus);
    assertOnFailure("7. Failed to test Agegate testing with valid age", bStatus);
    
    //Also it checks if home page loads
    dbgPrint("Opening Home page...");
    rptGen.assertResult("8. Home page loading ", bStatus);
    assertOnFailure("8. Failed to load home page", bStatus);
    /*
    //Check the age entered is displayed in registration form
    dbgPrint("Check the age entered is displayed in registration form...");
    clickOnJoinorLogin(driver);
    dbgPrint("111");
    isLoginWindowPopupSuccess(driver);
    dbgPrint("222");
    clickOnJoinNowButton(driver);
    dbgPrint("333");
    isJoinNowWindowPopupSuccess(driver);
    dbgPrint("444");
    bStatus = checkDOBValues(driver);
    dbgPrint("555");
    rptGen.assertResult("9. Age displaying in the registartion form ", bStatus);
    assertOnFailure("9. Failed to display age in registartion form", bStatus);
    
    */
    
    
    
    
    
  }
}