package test.techgentsia.testcases;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import test.techgentsia.common.BaseTest;
import test.techgentsia.common.HeinekenException;
import test.techgentsia.config.ConfigManager;

public class Heineken extends BaseTest
{
  
  private static final int WAIT_TIMEOUT = 20000; /* 0.5 minutes in terms of 500 milliseconds*/
  private String windowHandle;
   
  public Heineken( ConfigManager cfgMgr  )
  {
    /* Invoke the BaseTest Class constructor with  
    /* config manager and TAG */
    super(cfgMgr , "Heineken");
  }

  private void openHomeAction(WebDriver driver ,String baseURL )
  {
    driver.get(baseURL);
  } /* end openHomeAction() */


  private boolean verifyHomePage( WebDriver driver )
  { 
    boolean bStatus;
    By      byElement;
    
    byElement = (By)By.xpath(".//*[@id='username']");
    bStatus   = waitForPageToLoad( driver , byElement, WAIT_TIMEOUT );
    return bStatus;
    
   }/* end verifyHomePage() */

  private void loginEmptyFileds(WebDriver driver)
  {
    WebElement signIn;
    
    signIn      = driver.findElement(By.xpath("html/body/div[1]/form/div/button"));
    signIn.click(); 
    
  }
 
  private boolean verifyLoginEmptyFileds(WebDriver driver)
  {
    
    String errortoast;
    boolean bStatus;
    By      byElement;
    
    byElement = (By)By.xpath(".//*[@id='emhtml']/body/div[1]/form/div/div[1]/span");
    bStatus   = waitForPageToLoad( driver , byElement, WAIT_TIMEOUT );
    
    errortoast = driver.findElement(By.xpath(".//*[@id='emhtml']/body/div[1]/form/div/div[1]/span")).getText();
    dbgPrint(errortoast);
    
    if(errortoast.equals("Enter your username!"))
    {
      bStatus = true;
    }
    else
    {
      bStatus = false;
    }
    return bStatus;
  }
  
  private void loginUsernameOnly(WebDriver driver,String name)
  {
    WebElement userName;
    WebElement signIn;
    
    userName    = driver.findElement(By.xpath(".//*[@id='username']"));
    userName.sendKeys(name);
    
    signIn      = driver.findElement(By.xpath("html/body/div[1]/form/div/button"));
    signIn.click();  
  }
  
 
    
  
  @Override
  public void runTests() throws HeinekenException
  {
    WebDriver   driver;
    boolean     bStatus;

    /* Get a driver handle */
    driver  = browserInstance.getDriverInstance();
    
    //Open home page
    dbgPrint("Opening Home page...");
    openHomeAction(driver , cfgMgr.urlUnderTest2);
    dbgPrint("Waiting for home page to load");
    
    //check home page
    bStatus = verifyHomePage(driver);
    dbgPrint("Verified Home page");
    rptGen.assertResult("Home Page Loading ", bStatus);
    assertOnFailure("Failed to Load Home Page", bStatus);
    
    //Try login with empty user name and password fields
    loginEmptyFileds(driver);
    bStatus = verifyLoginEmptyFileds(driver);
    dbgPrint("completed login with empty fields");
    rptGen.assertResult("login with empty fields ", bStatus);
    assertOnFailure("Failed to login with empty fields", bStatus);    
    
    
  }
}