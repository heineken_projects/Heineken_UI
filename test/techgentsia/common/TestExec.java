package test.techgentsia.common;


import java.util.ArrayList;

import test.techgentsia.config.ConfigManager;
/**
 * This class implements the logic to manage a test.Following are the 
 * main functionalities
 * 1] Parses command line arguments and maps them to appropriate actions  
 * 2] Accepts test objects, executes them 
 * 3] Wait for the tests to finish before starting the next test
 * 
 * @author Juby George
 *
 */

public class TestExec
{
  private static String TAG = "EntryPoint";

  protected ArrayList<BaseTest>     testList;
  protected int                     currentTest;  
  protected int                     lastTest;
  
  
  
  protected TestExec()
  {
    testList    = new ArrayList<BaseTest>();
    /* Print the current version of test framework to console */
    dbgPrint(" Current Version :"+ ConfigManager.version );
  }
  

  /**
   * Utility function used for prints. 
   * @param str - The string to be printed
   */
  protected void dbgPrint( String str )
  {
    System.out.println( TAG + " : "+ str );
  }/* end dbgPrint() */

  /**
   * This function waits for the threads in the given lists to finish
   * 
   */
  protected void waitForTestsToFinish()
  {
    int      i;
    BaseTest testInstance;
    
    dbgPrint(" Waiting for Test to Finish ");
    
    /* Now wait for the tests to finish */
    for ( i = 0; i < testList.size(); i++ )
    {
      testInstance = testList.get(i);
      try
      {
        testInstance.join();
      } 
      catch (InterruptedException ex )
      {
        ex .printStackTrace();
      }
    }
    
    /* If some tests are stopped prematurely, 
     * clean them up */
    for ( i = 0; i < testList.size(); i++ )
    {
      testInstance = testList.get(i);
      testInstance.cleanUpTest();
    }
    dbgPrint(" Tests Finished!! ");
  }/* end waitForTestsToFinish() */

  
    
  
  /**
   * Add a test object to the execution list
   * @param testObject - The test class instance to be registered
   */
  protected void registerTest( BaseTest testObject )
  {
    testList.add( testObject );
    
  }/* end registerTest() */
  
  
  
  /**
   * Runs the registered tests concurrently
   */
  protected void runTests()
  {
    int       i;
    BaseTest  baseTest;

    for ( i = 0; i < testList.size(); i++ )
    {
      baseTest = testList.get(i);
      /* run test if its ready to run */
      if ( true == baseTest.isReadyToRun() )
      {
        baseTest.start();
      }
    }

  }/* end runTests() */
  
  
  /**
   * Process the command-line arugments passed to the system
   * and maps them to suitable actions
   * @param args - command-line arguments
   * @return - None
   */
  protected void processCommandLineArguments( String args[] )
  {
    boolean bStatus;
        
    if ( 1 == args.length )
    {
      String  cmdLineArg;

      cmdLineArg  = args[0]; 
      dbgPrint(cmdLineArg);
      
      if ( isInteger( cmdLineArg ))
      {
        currentTest  = Integer.parseInt(cmdLineArg);
        lastTest     = currentTest;   
        dbgPrint("TestNumber = "+currentTest);
        bStatus      = true;
      }
      else
      {
        bStatus     = false;
        if(cmdLineArg.toUpperCase().equals("HELP") || cmdLineArg.equals("?") )
        {
          displayHelp();
        }
        else if(cmdLineArg.toUpperCase().equals("LIST"))
        {
          displayTestList();
        }
        else
        {
          dbgPrint("Invalid argument!!!!");
        }
      }
    }
    else if( 0 == args.length )
    {
      bStatus       = true;
      currentTest   = 1;/*If no commandline arguments, execute tests from test-1*/
      lastTest      = EntryPoint.MAX_TEST_NUM;
    }
    else
    {
      bStatus = false;
    }
    
    if ( false == bStatus )
    {
      currentTest = EntryPoint.MAX_TEST_NUM + 1;
    }
  }/* end processCommandLineArguments() */
  
  
  /**
   * Displayes the registered tests along with the 
   * corrsponding test-id [test-number]
   */
  private void displayTestList()
  {
    dbgPrint("Not implemented ");
  }/* end displayTestList() */
  
  /**
   * Displays system help
   */
  private void displayHelp()
  {
    dbgPrint("Enter a single number in range 0 - "+ EntryPoint.MAX_TEST_NUM +"To run a selected test");
    dbgPrint("If all tests to be run, do not provide any command line arguments");
    
  }/* end displayHelp() */

  /**
   * Checks if the input is a number
   * @param s - String to verify
   * @return true if input is a number, false otherwise
   */
  private boolean isInteger(  String s  ) 
  {
    int     i;
    boolean bStatus;
    
    bStatus = true;
    
    if( s.isEmpty())
    {
      bStatus = false;  
    }
    
    if ( bStatus )
    {
      for(i = 0; i < s.length(); i++) 
      {
        if( (0 == i) && ( s.charAt(i) == '-') ) 
        {
          if(s.length() == 1)
          {
            bStatus = false;
            break;
          }
          else 
          {
            continue;
          }
        }
        if(Character.digit(s.charAt(i),10) < 0) 
        {
          bStatus = false;
          break;
        }
      }
    }
  
    return bStatus;
  }/*end isInteger() */
    
}
