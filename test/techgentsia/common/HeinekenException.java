package test.techgentsia.common;

import org.openqa.selenium.WebDriverException;

/**
 * Exception thrown by Heineken Framework.
 * @author Juby George
 *
 */

public final class HeinekenException extends WebDriverException
{
  /* To keep compiler happy*/
  public static final long serialVersionUID = 1;
  /**
   * Constructor
   * @param exceptionMessage - The message associated with the exception instance.
   */
  public HeinekenException( String exceptionMessage )
  {
    super();
    
  } /* end HeinekenException() */
  public void printStackTrace()
  {
    
    
  }

}
