package test.techgentsia.common;
import test.techgentsia.config.ConfigManager;
import test.techgentsia.testcases.*;

/**
 * This is the main entry point to this project. This class 
 * registers different tests to be executed and run them all
 * concurrently/sequentially as required using different 
 * threads. 
 * important :MAX_TEST_NUM must be incremented when a new 
 * test is added
 * @author Juby George
 */


public final class EntryPoint extends TestExec
{

  protected static final int  MAX_TEST_NUM  = 3; /* This variable should be updated when a new test is added */

  BaseTest        testObject;
  

  /**
   * Sample Test Staging
   */
  private void teststaging()
  {
    ConfigManager   cfgMgr;
    
    cfgMgr              = new ConfigManager();
    cfgMgr.browserType  = ConfigManager.CHROME;
    cfgMgr.urlUnderTest1 = "http://staging.hkn2.brandcommerce.io/";    
    testObject          = new Staging( cfgMgr );
    this.registerTest(  testObject );
  }/* end teststaging() */
  

 
  private void testheineken()
  { 
    
    ConfigManager   cfgMgr;
    cfgMgr                 = new ConfigManager();
    cfgMgr.browserType     = ConfigManager.CHROME;
    cfgMgr.urlUnderTest2    = "https://now.heineken.com/";
    testObject          = new Heineken(cfgMgr);
    this.registerTest(  testObject );
    
  } /*end testheineken */
  
   /**
   * This is the main entry point of the test framework
   * @param args - command-line arguments
   */
  public static void main( String args[] ) 
  {
    EntryPoint  entryPoint;
       
    entryPoint  = new EntryPoint();
    
    entryPoint.processCommandLineArguments((String[])args);
   
    while(entryPoint.currentTest <= entryPoint.lastTest )
    {
    
      switch( entryPoint.currentTest )
      {
        
        case 1:
          entryPoint.teststaging();
          break; 
          /*
        case 2:
          entryPoint.testheineken();
          break;     
          */   
        default:
          entryPoint.dbgPrint("Invalid Test Id provided, exiting... ");
          entryPoint.currentTest = entryPoint.lastTest;
          break;
      }/* switch( entryPoint.currentTest ) */
      
      /* Run the registered test*/
      entryPoint.runTests();
      /* Wait for current test['s] to finish*/
      entryPoint.waitForTestsToFinish();
      
      /* Move to next test, if applicable */
      entryPoint.currentTest++;
      
    }/*while(entryPoint.currentTest <= entryPoint.lastTest )*/
      
  } /* end main() */

}
