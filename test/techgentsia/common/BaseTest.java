package test.techgentsia.common;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import test.techgentsia.browser.BrowserManager;
import test.techgentsia.config.ConfigManager;
import test.techgentsia.utils.ReportGen;

/**
 * This class implements the common methods to be used by all the tests. 
 * Child test classes must override the method runTests() and implement all
 * the tests in it.
 * 
 * This class provides a synchronous and asynchronous interface to the test 
 * class. Synchronous interface is useful when a test should be done in a 
 * sequential way.Asynchronous interface uses the Thread interface. 
 * @author Juby George
 *
 */


public class BaseTest extends Thread 
{
  private static final int HALF_A_SECOND  = 500; /* half a second in milliseconds*/ 
  
  
  public String             TAG = null;
  public ConfigManager      cfgMgr;
  public ReportGen          rptGen;
  public BrowserManager     browserInstance;
  
  /**
   * This constructor is private intentionally, just to make sure that 
   * its not called by child classes. This is a way to make sure that 
   * child classes use the other constructor.
   */
  @SuppressWarnings("unused")
  private BaseTest()
  {
    
  }
  
  /**
   * Creates and instance of BastTest Class. This constructor must be invoked
   * by child classes using call to super(x,y)
   * @param cfgMgr - configuration to be used by the test
   * @param tag    - The tag name to be printed before each console print
   */
  public BaseTest ( ConfigManager cfgMgr, String tag  )
  {
    if ( null == cfgMgr )
    {
      try
      {
        throw new HeinekenException( " Valid Config required ");
      } 
      catch (HeinekenException ex )
      {
        ex.printStackTrace();
      }
    }
    this.cfgMgr = cfgMgr;
    TAG         = tag;
    
    /* Create report generator*/
    rptGen           = new ReportGen( cfgMgr, tag );
    /* Create a browser */
    browserInstance  = new BrowserManager( cfgMgr );
    
    if ( ( null == browserInstance ) || ( null == rptGen )  )
    {
      try
      {
        throw new HeinekenException( " Not able to create Browser or ReportGen Instance ");
      } 
      catch (HeinekenException ex )
      {
        ex.printStackTrace();
      }
    }
  }
  
  /**
   * This API performs a releases all the resources acquired by 
   * the test.
   * @return - None
   */
  private void closeTests()
  {
    /* Close the browser  */
    browserInstance.close();
    /* Close the report file  */
    rptGen.closeReportFile();
    
    browserInstance = null;
    rptGen          = null;
    cfgMgr          = null;
  }/* end closeTests() */
  
  private void SleepMillies( int numMiiles )
  {
    try
    {
      Thread.sleep( numMiiles );
    } 
    catch (InterruptedException e)
    {
      e.printStackTrace();
    }
  }/* end SleepMillies() */

  /****************************************************************************
   *              Methods to be used by child classes
   ****************************************************************************/

  /**
   * This API closes the test in a clean way. This API is called 
   * by the framework when all tests are finished. This is to make
   * sure that all the resources acquired are released gracefully.
   */
  protected void cleanUpTest()
  {
    if ( null != browserInstance )
    {
      dbgPrint("Test ");
      closeTests();
    }
  }/* end cleanUpTest() */
  
  /**
   * Child test classes must use this method to print to console.
   * @param str - The String to print
   */
  public void dbgPrint( String str )
  {
    if ( null ==  TAG )
    {
      try
      {
        throw new HeinekenException("Tag is not set. Please pass Child Class name as " +
                                          "second argument to BaseTest Constructor" );
      } 
      catch (HeinekenException ex)
      {
        ex.printStackTrace();
      }
    }
    
    System.out.println( TAG + " {"+cfgMgr.userName+"} : " +str );
  }/* end dbgPrint() */
  
  
  
  /**
   * The API checks if the given element is present in the current page
   * @param driver    - WebDriver instance
   * @param element   - The element to look for 
   * @return true if success, false otherwise
   */
  public boolean isElementPresent( WebDriver driver , By element ) 
  {
    boolean bStatus;
    try 
    {
      driver.findElement( element );
      bStatus = true;
    } 
    catch(  NoSuchElementException ex  ) 
    {
      bStatus =  false;
    }

    return bStatus;
  } /* end isElementPresent() */
  
  /**
   * This function waits for a page to load. The function makes sure that
   * the page is loaded by checking if the given element is displayed.    
   * @param driver  - The web driver instance
   * @param element - The element to look for in the destination page
   * @param timeOut - Maximum time to wait for page to load. If page is 
   *                  not loaded by this time, API will return with false result
   * @return - true if page is loaded successfully, flase otherwise
   */
  public boolean waitForPageToLoad( WebDriver driver , By element, int timeOut )
  {
    WebElement  wElement;
    boolean     bStatus;
    int         iCount;
    int         numSeconds;

    bStatus = false;
    iCount  = 0;
   
    do
    {
      //dbgPrint("Entering wait loop iteration = "+iCount);
      try
      {
        wElement  = driver.findElement( element );
        bStatus   = wElement.isDisplayed();
        //dbgPrint("Page Displayed status = "+bStatus);
        
      }
      catch( NoSuchElementException ex )
      {
        SleepMillies( 6000 );/* Just give other blocked threads an opportunity to run */
        //dbgPrint("NoSuchElementException");
        bStatus   = false;
      }
      catch ( StaleElementReferenceException ex )
      
      {
        SleepMillies( 1 );
        dbgPrint("StaleElementReferenceException");
        bStatus   = false;
      }
      catch ( ElementNotVisibleException ex )
      {
        bStatus = false;
        SleepMillies( 1 );
        dbgPrint("ElementNotVisibleException");
      }

      iCount++;

    }while( ( false == bStatus ) && ( iCount < timeOut ) );
    
    /* waiting till the element is found or
     * waiting for the timeout in case we did not get the element
     * bStatus becomes false , come out of the loop and the test fails 
     */
   
    ///dbgPrint("Num Iterations " + iCount );
    numSeconds  = iCount / 5;
    
    ///dbgPrint("Page took "+numSeconds + " seconds to load ");
    return bStatus; 
  } /* end waitForPageToLoad()  */
  
  
  /**
   * Wait for the given element to load and click on it
   * @param driver
   * @param element
   * @param timeOut
   * @return - true if successful, false otherwise
   */
  public boolean findAndClick( WebDriver driver , By element, int timeOut )
  {
    WebElement  wElement;
    boolean     bStatus;
    int         iCount;
    int         numSeconds;

    bStatus = false;
    iCount  = 0;
    
    do
    {
      try
      {
        wElement  = driver.findElement( element );
        if (wElement.isDisplayed())
        {
          if (wElement.isEnabled())
          {
            bStatus = true;
            wElement.click();
          }
        }
      }
      catch( NoSuchElementException ex )
      {
        bStatus = false;
        dbgPrint("NoSuchElementException");
      }
      catch ( StaleElementReferenceException ex )
      {
        bStatus = false;
        dbgPrint("StaleElementReferenceException");
      }
      catch ( ElementNotVisibleException ex )
      {
        bStatus = false;
        dbgPrint("ElementNotVisibleException");
      }
      
      if ( false == bStatus )
      {
        SleepMillies( 1 );
        iCount++;
      }
      
    }while( ( false == bStatus ) && ( iCount < timeOut ) );
    
    /* waiting till the element is found or
     * waiting for the timeout in case we did not get the element
     * bStatus becomes false , come out of the loop and the test fails 
     */
    numSeconds  = iCount / 5;
    
    //0dbgPrint("Element took "+numSeconds + " seconds to load ");
    return bStatus; 
  } /* end findAndClick()  */
  
  
  
  /**
   * Waits for the specified element and returns it
   * @param driver
   * @param element
   * @param timeOut
   * @return - The webelement requested if available. null otherwise
   */
  public WebElement getWebElement( WebDriver driver , By element, int timeOut )
  {
    WebElement  wElement;
    boolean     bStatus;
    int         iCount;
    int         numSeconds;
    bStatus = false;
    iCount  = 0;
    
    wElement = null;
    
    do
    {
      try
      {
        wElement  = driver.findElement( element );
        bStatus   = true;
      }
      catch( NoSuchElementException ex )
      {
        bStatus = false;
      }
      catch ( StaleElementReferenceException ex )
      {
        bStatus = false;
      }

      SleepMillies( 1 );
      iCount++;

    }while( ( false == bStatus ) && ( iCount < timeOut ) );
    
    /* waiting till the element is found or
     * waiting for the timeout in case we did not get the element
     * bStatus becomes false , come out of the loop and the test fails 
     */
   
    numSeconds  = iCount / 5;
    
    if ( false == bStatus || ( iCount == timeOut) )
    {
      wElement  = null;
      dbgPrint("Error: Timed out ");
    }
    
   // dbgPrint("Element took "+numSeconds + " seconds to load ");
    return wElement; 
  } /* end getWebElement()  */
  
  

  
  
  /**
   * This API puts system in sleep for the time specified 
   * @param numHalfSeconds - time in unit of 0.5 seconds
   */
  public void Sleep( int numHalfSeconds )
  {
    try
    {
      Thread.sleep( numHalfSeconds * HALF_A_SECOND );
    } 
    catch (InterruptedException e)
    {
      e.printStackTrace();
    }
  }/* end Sleep() */
  
  /**
   * This function throws a HeinekenException with the given message 
   * if the given condition is evaluated as false.This function should 
   * be called when the test can not proceed if an action is failing.
   * @param message     - The message to be included in the exception
   * @param condition   - The condition to be checked 
   * @throws HeinekenException
   * @return None
   */
  public void assertOnFailure( String message, boolean condition ) throws HeinekenException
  {
    if (!condition)
    {
      
      throw new HeinekenException( cfgMgr.userName +" : " +message );
    }
  }/* end assertOnFailure() */
  
  
  /**
   * This method is used to check if the test object is ready to run
   * @return true, if test is ready to run, false otherwise
   */
  public boolean isReadyToRun()
  {
    boolean bStatus;

    if ( null != browserInstance )
    {
      bStatus = true;
    }
    else
    {
      bStatus = false;
    }
    return bStatus;
  }/* end isReadyToRun() */

  /****************************************************************************
   *              Methods to be overridden by child classes
   ****************************************************************************/
  
  /**
   * This method runs all the tests and logs results.
   * The child test class must override this method and all the
   * tests must be implemented in this method. 
   */
  public void runTests() throws HeinekenException
  {
    /* Assert an exception to inform that child class has not overridden this method */
    try
    {
      throw new HeinekenException(" TestClass Must override the method runTests() ");
    } 
    catch (HeinekenException ex)
    {
      ex.printStackTrace();
    }
  } /* end runTests()  */

  /****************************************************************************
   *                  Sequential Interface
   ****************************************************************************/
    
  
  /**
   * This function executes all the tests and  closes the resources acquired 
   * once the test is finished. This is a blocking call, ie. a call to this API will 
   * return only after finishing the tests and closing the resources.
   * Entry point should use this method if the test should be done in a sequential way
   */
  public void executeTests()
  {
    /* Call the tests*/
    try
    {
      runTests();
    } 
    catch (HeinekenException ex)
    {
      dbgPrint(" Test Stopped Prematurely with the following mesage :\"" + ex.getMessage()+"\" ");
      ex.printStackTrace();
    }
    /* Closes the resources */
    closeTests();
  }/* end executeTests() */
  
  
  /****************************************************************************
   *                Thread Interface
   ****************************************************************************/

  /**
   * This is the thread interface. child classes must not override or 
   * call this method directly
   */
  @Override
  public void run() 
  {
    executeTests();
  }/* end run()*/
  
}
