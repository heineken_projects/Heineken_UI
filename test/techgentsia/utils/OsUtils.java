package test.techgentsia.utils;

import java.nio.file.Paths;
import java.nio.file.Path;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * This class implements OS specific utilities  
 * @author Juby George
 * */
public final class OsUtils
{
  private static String  OS           = null;/*OS string*/
  private static boolean bIs64bitOs   = false;
  
  public static String getOsName()
  {
    if(OS == null) 
    { 
      OS = System.getProperty("os.name"); 
    }
    return OS;
  }/* end getOsName() */
   
  public static boolean isWindows()
  {
    boolean bIsWindows ;

    bIsWindows = getOsName().startsWith("Windows");

    if ( true == bIsWindows )
    {
      if (System.getProperty("os.name").contains("Windows")) 
      {
        bIs64bitOs = (System.getenv("ProgramFiles(x86)") != null);
      } 
      else 
      {
        bIs64bitOs = (System.getProperty("os.arch").indexOf("64") != -1);
      }
    }

    return bIsWindows;
  }/* end isWindows() */

  public static boolean isLinux()
  {
    boolean bIsLinux ;
    
    bIsLinux  = getOsName().startsWith("Linux");
    
    if ( true == bIsLinux )
    {
      bIs64bitOs = is64BitLinux();
    }

    return bIsLinux ;
  }/*end isLinux()*/
  
  /**
   * Tells if OS is 32 bit or 64 bit
   * @return - true if 64 bit, false otherwise
   */
  public static boolean isOs64Bit()
  {
    return bIs64bitOs;
  }/* end isOs64Bit() */
  
  /**
   * This function converts the given path to native format
   * @param path - Path to be converted
   * @return - Path string in the native format
   */
  public static String convertToNativePath( String path )
  {
    Path   pPath;
    String sPath;
    
    pPath = Paths.get( path );

    sPath = pPath.toString();
    
    return  sPath;
  }/* end convertToNativePath() */
  
  /**
   * Checks if linux is 64 bit
   * @return true if 64bit false otherwise
   */
  private static boolean is64BitLinux()
  {
    String command = "uname -i";

    String output = executeOsCommand( command );

    boolean is64bit = output.equals("x86_64\n");
   
    return is64bit;
  }/* end is64BitLinux() */
  
  /**
   * This API executes an OS command and returns the result as a string
   * @param command - OS command to be executed
   * @return - result of the command
   */
  private static String executeOsCommand( String command ) 
  {
    StringBuffer output = new StringBuffer();

    Process p;
    try 
    {
      p = Runtime.getRuntime().exec(command);
      p.waitFor();
      BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
      
      String line = "";
      while ((line = reader.readLine())!= null) 
      {
      output.append(line + "\n");
      }

    } 
    catch (Exception ex) 
    {
      ex.printStackTrace();
    }

    return output.toString();
  }/* end executeOsCommand() */


}/* end OsUtils */