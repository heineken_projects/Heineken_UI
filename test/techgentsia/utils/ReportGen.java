package test.techgentsia.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import test.techgentsia.config.ConfigManager;

/**
 * This class implements report generation.  
 * @author Juby George
 * */
public final class ReportGen
{
  private static String TAG = "ReportGen";
  private PrintWriter   writer;
    
  /**
   * Constructor for Report Generator
   * @param cfgMgr    - Configuration manager
   * @param testTag   - Name of the test
   */
  public ReportGen( ConfigManager cfgMgr, String testTag )
  {
    /* Test report file name is of the format
     * TestTag_UserName_reportFileName
     * */
    openReportFile( cfgMgr.reportFolder + testTag+"_" +cfgMgr.userName+"_" + cfgMgr.reportFile );
    
    /* Print the Report Heading line as the first line */
    dbgPrint("Test Case , Test Result");
  }
    
  /**
   * Utility function used for prints. 
   * @param str - The string to be printed
   * @return - None
   */
  private void dbgPrint( String str )
  {
    System.out.println(TAG + " : " + str );
    dbgPrintToReportfile( str );
  }/* end dbgPrint(0 */
  
  /**
   * This function opens the report file
   * @param fileName - Name of the Report file
   */
  private void openReportFile( String fileName )
  {
    try
    {
      writer = new PrintWriter(new FileWriter( fileName , false)); 
    } 
    catch ( IOException ex )
    {
      ex.printStackTrace();
      writer  = null;
      dbgPrint(" Failed to Create Report File");
    } 
  }/* end openReportFile() */
  
  
  
  /**
   * Prints the given string to file
   * @param str - String to be printed to file
   */
  private void dbgPrintToReportfile( String str )
  {
    if ( null != writer )
    {
      writer.println(str);
    }
    else
    {
      System.out.println( "( null == writer )" );
    }
  } /* end dbgPrintToReportfile() */
  
  
  /**
   * Closes the report file
   */
  public void closeReportFile()
  {
    if ( null != writer )
    {
      writer.flush();
      writer.close();
    }
  }/* end closeReportFile() */
  
  
  /**
   * Interface for the test programs to log the results of each test
   * to the report file
   * @param statusMessage - Message to be logged
   * @param bStatus       - true if test is success, false otherwise
   */
  public void assertResult( String statusMessage ,boolean bStatus )
  {
    if ( bStatus )
    {
      dbgPrint(statusMessage + " , SUCCESS " );
    }
    else
    {
      dbgPrint( statusMessage +  " , FAILED " );  
    }
  }/* end assertResult() */
}
