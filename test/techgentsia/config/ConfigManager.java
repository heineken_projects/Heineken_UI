package test.techgentsia.config;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import test.techgentsia.common.HeinekenException;

/**
 * This class holds the test configuration. All the parameters to the test 
 * is stored here or in a derived class of this . A configured instance of 
 * this class is used by the test class and test-framework to drive the test.
 * 
 * TODO: In the near future a JSON parser module will load these parameters 
 * in run time from a JSON file. 
 * 
 * @author Juby George
 */

public class ConfigManager
{
  
  /* Current version of the test framework */
  public static final String version      = "0.9";
  
  public static final int MIN_BROWSER = 0;
  public static final int CHROME      = 1; /* Google Chrome Browser     */
  public static final int FIREFOX     = 2; /* Mozilla Firefox Browser   */
  public static final int IEXPLORER   = 3; /* Internet Explorer Browser */
  public static final int MAX_BROWSER = IEXPLORER + 1;

  public static final String browserProfileLocation = "BrowserProfile"; /* Folder where browser profiles are created */
  public static final String JSON_CONFIG_FOLDER         = "./config/";
  public static final String DEFAULT_JSON_CONFIG_FILE   = "DefaultConfig.json";
  
  /* Entry URL to be tested */
  public String urlUnderTest1;
  public String urlUnderTest2;
  public String userName;
  public String password;
  
  /* Folder where reports are to be kept */
  public String reportFolder;
  /* File to which report to be printed */
  public String reportFile;
  public int  browserType;
  

  
  public ConfigManager()
  {
    urlUnderTest1  = null;
    urlUnderTest2  = null;
    reportFolder  = "./";
    reportFile    = "TestReport.csv";
    browserType   = MIN_BROWSER;
  }
  
}

