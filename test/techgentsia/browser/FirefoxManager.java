package test.techgentsia.browser;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;



/**
 * This class abstracts all the Firefox browser related details
 * @author Juby George
 *
 */
final class FirefoxManager extends BrowserInterface
{
  private static String TAG = "FirefoxManager";
  
  private static String fireFoxProfileFolder = "TechGentsiaFirefoxProf";
 

  protected FirefoxManager( String browserProfileLoc )
  {
    userProfile = browserProfileLoc + fireFoxProfileFolder;
  }

  /**
   * Utility function used for prints. 
   * @param str - The string to be printed
   */
  private void dbgPrint( String str )
  {
    System.out.println( TAG + " : " + str );
  }/* end dbgPrint() */
  

  @Override
  protected WebDriver getDriver()
  {
    WebDriver driver;
    
    FirefoxProfile firefoxProfile  = getFirefoxProfile();
    firefoxProfile.setPreference("fake", true);
 
    if ( null != firefoxProfile)
    {
      driver = new FirefoxDriver(firefoxProfile);
    }
    else
    {
      dbgPrint("Error : Firefox Profile Creation failed ");
      driver = new FirefoxDriver();
    }

    return driver;
  } /* end getDriver() */

  private FirefoxProfile getFirefoxProfile()
  {
    File            firfoxProfFolder;
    FirefoxProfile  firefoxProfile;
    boolean         result;

    result      = true;
    

    firfoxProfFolder = new File(userProfile);
    
    if (!firfoxProfFolder.exists()) 
    {
      dbgPrint("Creating Firefox Profile Folder : " + userProfile );

      try
      {
        firfoxProfFolder.mkdir();
        result = true;
      }
      catch(SecurityException ex )
      {
        result = false;
        ex.printStackTrace();
      }


    }

    if( result ) 
    {
      dbgPrint("Firefox Profile loaded from :"+userProfile);

      firefoxProfile = new FirefoxProfile(new File(userProfile));

      firefoxProfile.setPreference("media.navigator.permission.disabled", true);
      firefoxProfile.setPreference("fake", true);

    }
    else
    {
      firefoxProfile = null;
    }
    return firefoxProfile;
  }/* end getFirefoxProfile() */


}/* end class FirefoxManager() */
