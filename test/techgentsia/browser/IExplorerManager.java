package test.techgentsia.browser;

import org.openqa.selenium.WebDriver;

import test.techgentsia.common.HeinekenException;

/**
 * This class handles all the Internet Explorer specific 
 * details.
 * @author Juby George
 *
 * TODO: IE support to be implemented 
 */

final class IExplorerManager extends BrowserInterface
{
  private static String TAG = "IExplorerManager";
  
  public IExplorerManager( String browserProfileLoc )
  {
    
  }
  
  @Override
  protected WebDriver getDriver()
  {
    try
    {
      throw new HeinekenException( TAG +" Browser Not supported");
    } catch (HeinekenException ex)
    {
      ex.printStackTrace();
    }
    return null;
  } /* end getDriver() */
  
}
