package test.techgentsia.browser;

import java.io.File;

import org.openqa.selenium.WebDriver;

import test.techgentsia.common.HeinekenException;

/* This abstract class is here to force the child browser classes to 
 * use a unique interface
 * */
public class BrowserInterface
{
  private static String TAG = "BrowserInterface";
  protected String   userProfile;
  
  /**
   * Browser classes MUST Override this method
   * @return WebDriver Instance
   */
   
  protected WebDriver getDriver()
  {
    try
    {
      throw new HeinekenException(" Browser Class Must override the method getDriver() ");
    } 
    catch (HeinekenException ex)
    {
      ex.printStackTrace();
    }
    return null;
  }/* end getDriver() */
  
  /**
   * This function removes the browser profile created
   * @return true, if successful, false otherwise
   */
  protected boolean removeProfile() 
  {
    boolean bStatus;
    File    profileFolder;

    dbgPrint("Removing Old Browser Profile : "+ userProfile );
    profileFolder = new File( userProfile );

    deleteProfile( profileFolder );

    bStatus = profileFolder.delete();
    
    return bStatus;
  }/* end removeProfile() */
 
 
  
  /**
   * This function traverses the sub-folders of the given folder
   * and removes all the files and sub-folders, and finally removes 
   * the top level folder given. Please note that this function is
   * recursive. so there are multiple return-statements.
   * Please do not modify this function if not necessary.
   * @param profileFolder
   * @return true, if successful, false otherwise
   */
  private  boolean deleteProfile( File profileFolder ) 
  {
    boolean  bStatus;
    String[] list;
    
    bStatus = true;

    if ( profileFolder.exists() )
    {
      if ( null == profileFolder || false == profileFolder.isDirectory() )
      {
        bStatus = false;
      }

      if ( true == bStatus )
      {
        list = profileFolder.list();

        if (list != null) 
        {
          for (int i = 0; i < list.length; i++) 
          {
            File entry = new File(profileFolder, list[i]);

            if (entry.isDirectory())
            {
              if (!deleteProfile(entry))
                return false;
            }
            else
            {
              if (!entry.delete())
                return false;
            }
          }
        }
      }/* if ( true == bStatus ) */

      bStatus = profileFolder.delete(); 
    }/* if ( profileFolder.exists() ) */

    return bStatus;
  }/* end deleteProfile() */



  /**
   * Utility function used for prints. 
   * @param str - The string to be printed
   */
  private void dbgPrint( String str )
  {
    System.out.println(TAG + " : " + str );
  }/* end dbgPrint() */
}
