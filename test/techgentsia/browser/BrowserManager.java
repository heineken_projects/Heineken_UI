package test.techgentsia.browser;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;

import test.techgentsia.common.HeinekenException;
import test.techgentsia.config.ConfigManager;
import test.techgentsia.utils.OsUtils;

/**
 * This class manages the different types of browsers supported and
 * provide a single interface to the test layer
 * @author Juby George
 */

public class BrowserManager
{
  private static String TAG = "BrowserManager";
  
  //sarika
  private static String OS = System.getProperty("os.name").toLowerCase();
  //sarika

  private WebDriver         driverInstance;
  private BrowserInterface  browserInstance;
  private String            browserProfileFolder; /*Folder where all browser profiles are created */

  /**
   * 
   * @param cfgMgr - Configuration to use
   */
  public BrowserManager( ConfigManager cfgMgr )
  {
    String userHome = System.getProperty("user.home");
    
   
    
    
    /* Make a folder for browser profiles profiles */
    //this is for windows
    //browserProfileFolder  = userHome+ "\\"+ ConfigManager.browserProfileLocation + "\\"+cfgMgr.userName + "_";

    //this is for linux
    browserProfileFolder  = userHome+ "/"+ ConfigManager.browserProfileLocation + "/"+cfgMgr.userName + "_";
    
    switch( cfgMgr.browserType )
    {
      case ConfigManager.CHROME:
        browserInstance = new ChormeManager( browserProfileFolder );
        driverInstance  = browserInstance.getDriver();
        break;
      case ConfigManager.FIREFOX:
        browserInstance = new FirefoxManager( browserProfileFolder );
        driverInstance  = browserInstance.getDriver();
        break;
      case ConfigManager.IEXPLORER:
        browserInstance = new IExplorerManager( browserProfileFolder );
        driverInstance  = browserInstance.getDriver();
      default:
        dbgPrint("ERROR : Unknown Browser Type");
        try
        {
          throw new HeinekenException("ERROR : Unknown Browser Type");
        } 
        catch ( HeinekenException ex )
        {
          ex.printStackTrace();
        }
        break;
    }
    /* Clearing profiles from previous run's if something is left.
     * */
    browserInstance.removeProfile();
  }

  
  /**
   * Utility function used for prints. 
   * @param str - The string to be printed
   * @return - None
   */
  private void dbgPrint( String str )
  {
    System.out.println( TAG + " : " + str );
  }/* end dbgPrint() */
  
  /**
   * This function closes the unwanted tabs like default welcome tab
   * for the browser.
   * @param   - none
   * @return  - none
   */
  private void closeUnwantedTabs()
  {
    ArrayList<String> tabs;
    
    tabs = new ArrayList<String> (driverInstance.getWindowHandles());
    
    while ( tabs.size() > 1 )
    {
      driverInstance.switchTo().window(tabs.remove(1));
      driverInstance.close();
    }
    driverInstance.switchTo().window(tabs.remove(0));
  }/* end closeUnwantedTabs() */
  

  /**
   * Returns an instance of the webDriver for the 
   * Browser of choice
   * @return - WebDriver instance 
   */
   
  public WebDriver getDriverInstance()
  {
    closeUnwantedTabs();
    return driverInstance;
  }/* end getDriverInstance() */

  
  /**
   * Closes the browser window
   */
    
  public void close()
  {
    String            tabHandle;
    ArrayList<String> browserTabs;
    
    /* When opening for the first time, some browsers may open welcome tab,
     * we should close all the tabs here to clean the profile in next step */
    browserTabs = new ArrayList<String> (driverInstance.getWindowHandles());
    do
    {
      tabHandle = browserTabs.remove(0);
      driverInstance.switchTo().window( tabHandle );
      driverInstance.close();
    }while( !browserTabs.isEmpty());
   
    driverInstance.quit();
    
    browserInstance.removeProfile();
  }/* end close() */


  /**
   * Maximises the Browser window
   */
  public void windowMaximise()
  {
    ///driverInstance.manage().window().maximize();
  } /* end windowMaximise() */

}
