package test.techgentsia.browser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import test.techgentsia.common.HeinekenException;
import test.techgentsia.utils.OsUtils;


/**
 * This class implements all the Google-Chrome specific settings 
 * @author Juby George
 */
 
final class ChormeManager extends BrowserInterface
{
  private static String TAG = "ChormeManager";
  private static String chromeDriverPath = "/asset/";
  private String chromeDrvBinName;/* Name of the binary*/
  
  /* The name of the folder where Chrome should create a custom profile for
   * This test framework */
  private static String chromeProfileFolder = "TecgGensiaChromeProf";
    
  /**
   * 
   * @param browserProfileLoc - Location where browser profiles to be created
   */
  protected ChormeManager( String browserProfileLoc ) throws HeinekenException
  {
    String  chromeDriverPathStr;
    String  currentDirStr = System.getProperty("user.dir");
    System.out.println("Current dir using System:" +currentDirStr);

    /* Make a folder for Google chrome's profile*/ 
    userProfile     = browserProfileLoc + chromeProfileFolder; 
    dbgPrint(" Loading Chrome Profile " +userProfile); 
     
   // System.setProperty("user.dir"); 

    /* Choose the chrome driver to be used  */
    chooseChromeDriver();
    /* Construct the full path [in relevant OS supported format ]   to selected chrome driver */
    chromeDriverPathStr = OsUtils.convertToNativePath( currentDirStr +"/"+chromeDriverPath+"/"+chromeDrvBinName );

    /* Tell selenium where to find chrome-driver binary */
    System.setProperty("webdriver.chrome.driver", chromeDriverPathStr );
  }/* end ChormeManager() */

  /**
   * Chooses the chrome driver to be loaded based on OS 
   */
  private void chooseChromeDriver() throws HeinekenException
  {
    /**
     * TODO: Detect the OS and based on that select the executable name here
     */
    dbgPrint(" OPERATING SYSTEM :  "+System.getProperty("os.name"));

    if ( OsUtils.isWindows())
    {
      dbgPrint(" Choosing Chrome Driver for Windows 32-bit");
      chromeDrvBinName = "chromedriver_win.exe";
    }
    else if ( OsUtils.isLinux())
    {
      if ( OsUtils.isOs64Bit() )
      {
        dbgPrint(" Choosing Chrome Driver for Linux 64-bit");
        chromeDrvBinName = "chromedriver_lin_64";
      }
      else
      {
        dbgPrint(" Choosing Chrome Driver for Linux 32-bit");
        chromeDrvBinName = "chromedriver_lin_32";
      }
    }
    else
    {
      throw new HeinekenException("Detected unsupported Operating system : "+ OsUtils.getOsName());
    }

  }/* end chooseChromeDriver() */


  
  /**
   * Utility function used for prints. 
   * @param str - The string to be printed
   */
  
private void dbgPrint( String str )
  {
    System.out.println( TAG + " : " + str );
  }/* end dbgPrint() */


  

  private DesiredCapabilities setChromeProfile()
  {
    DesiredCapabilities   capabilities;
    ChromeOptions         options;

    capabilities  = DesiredCapabilities.chrome();
    options       = new ChromeOptions();
    options.addArguments( "user-data-dir="+ userProfile );
    options.addArguments( "--start-maximized" );
    /* Instruct Chrome to use browser simulated webcam and Mic */
    options.addArguments( "--use-fake-device-for-media-stream" );
    /* Instruct Chrome not to prompt for permission dialog */
    options.addArguments( "--use-fake-ui-for-media-stream" );
    
    capabilities.setCapability(ChromeOptions.CAPABILITY, options);
    
    return capabilities;
  }/* end setChromeProfile() */
  
  
  @Override
  protected WebDriver getDriver()
  {
    WebDriver   driver; 

    DesiredCapabilities  capabilities;
    
    capabilities  = setChromeProfile();
    
    if ( null != capabilities )
    {
      driver = new ChromeDriver(capabilities);
    }
    else
    {
      dbgPrint("Error : Google Chrome Profile Creation failed ");
      driver  =  new ChromeDriver();
    }
    
    return driver;
  }/* end getDriver() */
  
  
}/* end class ChormeManager */
