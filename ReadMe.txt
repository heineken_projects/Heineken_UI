Prerequesties 
===============
1] javaSE 1.8 or later installed

2] Eclipse for Java installed

3] Download selenium for java from the url below and extract it to <Install-folder>
   http://selenium-release.storage.googleapis.com/2.53/selenium-java-2.53.1.zip

   4] Download ChromeDriver [Google-chrome browser's driver for selenium] form the url below and extract it to  <Install-folder>
   http://chromedriver.storage.googleapis.com/2.22/chromedriver_win32.zip  
   
5] Add <Install-folder>\chromedriver_win32 to windows's "system path"

   
Steps to use the test framework 
================================
1] Check-out the repository to <WorkFolder>

2] Open the project
   Open a java project in eclipse and give the path <WorkFolder>\WorkSpace
   This should open the project in eclipse
   
3] Import Selenium support
   From eclipse projet, follow the following path and import all the jar files in sellenium folder <Install-folder>
   Project->Properties->Java Build Path->Libraries -> Add Externel JAR's
    
4] Import Java Coding Style 
   Import the <WorkFolder>/EclipseCodeFormattingPolicies/JavaCodingStyle.xml as below   
   Project->Properties -> Java code Style->Formatter->Import 
   
   Note: 
   This .xml file has the information regarding TAB's or spaces and how many spaces...etc.
   
   Please follow the coding style used here if there are no company prefered coding style.
   If there is a prefered coding style, please share it.
   
   
Project Structure
==================
The test framework is organised in to the following 5 packages

1] test.techgentsia.browser
2] test.techgentsia.common
3] test.techgentsia.config
4] test.techgentsia.testcases
5] test.techgentsia.utils   

1] test.techgentsia.browser
   All the browser specific code goes here. There will be a seperate class for each new browser supported.
   The top level class is BrowserManager.java which is the interface to this package
   
2] test.techgentsia.common
    All the common files goes here. EntryPoint.java is the entry-point to this project.
    
3] test.techgentsia.config
   All the settings goes here. currently all the values are hard-coded here. Plan is to load the settings values
   from a JSON file. This is to avoid compilation. A seperate JASON parser module needs to be developed. This is <TODO>
   
4] test.techgentsia.testcases
   The test cases goes here. Each web project should have one or more classes here. These classes have the website 
   specific test logic. Please note that test-case class has got a method runTests() thats implemented form the 
   interface TestInterface.java. All the futre test-class added here should implement TestInterface and extend 
   BaseTest.java class. BaseTest is a collection of methods to be shared by all tests.
   Please note that each test class must call the method setTag() from its constructor with the argument as the name
   of the test-class as a string.
   
5] test.techgentsia.utils   
   All the utilitiy classes are kept here. currently only ReportGen.java is avaialbe here. It takes care of
   Report Generation.
   
  
Output folder
=============
Compiler output goes to ./out


Coding Style Recomendations
============================
1] Please follow the style of coding followed here, unless if there are any company followed styles.
2] Write comments for functions, variables and even within code so that its easy to understand the code.
3] I have not used System.out.print() directly in the code, insted I have a private function "void dbgPrint( String str )"
   in each class. This is to help adding a TAG to all the prints. The TAG used is class-name. This helps debugging a lot 
   in future.


Running
========
When running, the testframework creates a folder with name "BrowserProfile" in the "current-user-home"
All the browser profiles will be kept here. Test framework creates a seperate browser profile for running tests.
This is imporant because test environemtn needs browser to be configured in certain ways.



